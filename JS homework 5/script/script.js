/*Порядок выполнения кода:
1) создаётся обьект Документ с вложенными обьектами
2) пишем заголовок,
3) вызываем функцию fillInDocument. 
4) функция fillInDocument заполняет объект-родитель и выводит на экран
5) вызываем функцию fillInApplication
6) функция fillInApplication заполняет вложенные в application обьекты


*/
const Document1 = {
    header: "",
    body: "",
    footer: "",
    date: "",
    application: {
        header: {},
        body: {},
        footer: {},
        date: {}
    }
};
document.write(`<h1> Document1 </h1>`);

function fillInDocument(header, body, footer, date) {
    Document1.header = header;
    Document1.body = body;
    Document1.footer = footer;
    Document1.date = date;
    document.write(`<p>header : <span>"${Document1.header}"</span> </p>`);
    document.write(`<p>body : <span>"${Document1.body}" </span></p>`);
    document.write(`<p>footer : <span>"${Document1.footer}" </span></p>`);
    document.write(`<p>date : <span>"${Document1.date}" </span></p>`);  
    document.write(`application :`);
}
function fillInApplication(header,body,footer,date) {
    Document1.application.header = header;
    Document1.application.body = body;
    Document1.application.footer = footer;
    Document1.application.date = date;
    document.write(`<p> &nbsp header : <span>"${Document1.application.header}"</span> </p>`);
    document.write(`<p>&nbsp body : <span>"${Document1.application.body}"</span> </p>`);
    document.write(`<p>&nbsp footer : <span>"${Document1.application.footer}"</span> </p>`);
    document.write(`<p>&nbsp date : <span>"${Document1.application.date}"</span> </p>`); 
}
fillInDocument("thisisheader", "thisisbody", "thisisfooter", "thisisdate");
fillInApplication("typeyourheader", "typeyourbody", "typeyourfooter", "typeyourdate");
