/*Задание 1*

Реализовать слайдер на чистом Javascript.

#### Технические требования:
- Создать HTML разметку, содержащую кнопки `Previous`, `Next`, и картинки (6 штук),
 которые будут пролистываться горизонтально. 
- На странице должна быть видна только одна картинка. Слева от нее будет кнопка 
`Previous`, справа - `Next`.
- По нажатию на кнопку `Next` - должна появиться новая картинка, следующая из списка.
- По нажатию на кнопку `Previous` - должна появиться предыдущая картинка.
- Слайдер должен быть бесконечным, т.е. если в начале нажать на кнопку `Previous`, 
то покажется последняя картинка, а если нажать на `Next`, когда видимая - последняя 
картинка, то будет видна первая картинка.
- Пример работы слайдера можно увидеть
(http://kenwheeler.github.io/slick/) (первый пример). */

window.addEventListener("DOMContentLoaded", () => {

// находим кнопки
let next = document.querySelector(".next");
let previous = document.querySelector(".previous")

// создаём массив слайдов и точки под картинками
const slider = document.querySelectorAll("img");
const dots = document.querySelectorAll("li")

// создаём перелистывание картинок вправо
let i = 0;
slider[i] = dots[i]

next.addEventListener("click", function() {
    next.style.outline="none";
    ++i
    //Правило для последнего слайда
            // Если переменная i больше или равна количеству слайдов
            // то удаляем класс block у предыдущего слайда
    if (i >= slider.length) {
        slider[i-1].classList.remove("block");
        dots[i-1].style.backgroundColor="rgb(116, 116, 128)"
        // Обнуляем переменную
        i = 0;
        // Добавляем класс block следующему слайду
        slider[i].classList.add("block");
        dots[i].style.backgroundColor="rgb(65, 65, 93)"
    } else { 
    //Правило для всех остальных слайдов   
        // Удаляем класс block предыдущему слайду
        // Добавляем класс block следующему слайду
        slider[i-1].classList.remove("block");
        dots[i-1].style.backgroundColor="rgb(116, 116, 128)"
        slider[i].classList.add("block");
        dots[i].style.backgroundColor="rgb(65, 65, 93)"
    }
})
//перелистывание влево
previous.addEventListener("click", function() {
    previous.style.outline="none";
   
    //Правило первого слайда
    if (i<=0) {
        slider[i].classList.remove('block');
        dots[i].style.backgroundColor="rgb(116, 116, 128)"
        i= slider.length;
        slider[i - 1].classList.add("block");
        dots[i-1].style.backgroundColor="rgb(65, 65, 93)"
        --i
    }
    else {
    //Правило для всех остальных слайдов   
        // Удаляем класс block предыдущему слайду
        // Добавляем класс block следующему слайду
        slider[i].classList.remove("block");
        dots[i].style.backgroundColor="rgb(116, 116, 128)"
        slider[i - 1].classList.add("block");
        dots[i-1].style.backgroundColor="rgb(65, 65, 93)"
        --i
    }
})
}) 

