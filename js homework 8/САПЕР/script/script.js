//САПЕР

document.addEventListener('DOMContentLoaded', () => {
    const grid = document.querySelector('.grid')
    const flagsLeft = document.querySelector('#flags-left')
    const bombAmountTotal = document.querySelector("#bombAmountTotal")
    const result = document.querySelector('#result')
    let width = 8
    let bombAmount = 10
    let flags = 0
    let squares = []
    let isGameOver = false
    let totalClicks = 0;
  
    //создаем игровое поле
    function createBoard() {
      flagsLeft.innerHTML = bombAmount
      bombAmountTotal.innerHTML = bombAmount
  
      //создаем массив бомб и пустых ячеек и смешиваем
      
      const bombsArray = Array(bombAmount).fill('bomb')
      const emptyArray = Array(width*width - bombAmount).fill('valid')
      const gameArray = emptyArray.concat(bombsArray)
      const shuffledArray = gameArray.sort(() => Math.random() -0.5)
      
  
       //создаем массив ячеек и присваиваем каждой уникальный id,

      for(let i = 0; i < width*width; i++) {
        const square = document.createElement('div')
        square.setAttribute('id', i)
        square.classList.add(shuffledArray[i])
        grid.appendChild(square)
        squares.push(square)
  
        //функция для левого клика
        square.addEventListener('click', function(e) {
          click(square)
        })




        
  
        //функция для правого клика
        square.oncontextmenu = function(e) {
          e.preventDefault()
          addFlag(square)
        }
      }
  
      //add numbers

      //находим левый и правый край игрового поля
      for (let i = 0; i < squares.length; i++) {
        let total = 0
        const isLeftEdge = (i % width === 0)
        const isRightEdge = (i % width === width -1)
  
        //добавляем инфломацию о том, сколько бомб в соседних ячейках
        if (squares[i].classList.contains('valid')) {
          //проверка ячейки справа в том же ряду
          if (i > 0 && !isLeftEdge && squares[i -1].classList.contains('bomb')) total ++
          //проверка ячейки слева в том же ряду
          if (i > 7 && !isRightEdge && squares[i +1 -width].classList.contains('bomb')) total ++
          // проверка ячейки над выбранной
          if (i > 8 && squares[i -width].classList.contains('bomb')) total ++
          //проверка ячейки слева в верхнем ряду от выбранной
          if (i > 9 && !isLeftEdge && squares[i -1 -width].classList.contains('bomb')) total ++
          //проверка ячейки справа от выбранной
          if (i < 62 && !isRightEdge && squares[i +1].classList.contains('bomb')) total ++
          // проверка ячейки слева внизу от выбранной
          if (i < 56 && !isLeftEdge && squares[i -1 +width].classList.contains('bomb')) total ++
          // проверка ячейки справа внизу от выбранной
          if (i < 54 && !isRightEdge && squares[i +1 +width].classList.contains('bomb')) total ++
          //проверка ячейки внизу под выбранной
          if (i < 55 && squares[i +width].classList.contains('bomb')) total ++
          squares[i].setAttribute('data', total)
        }
      }
    }
    createBoard()
  
    //расставляем флажки
    function addFlag(square) {
      if (isGameOver) return
      if (!square.classList.contains('checked') && (flags < bombAmount)) {
        if (!square.classList.contains('flag')) {
          square.classList.add('flag')
          square.innerHTML = ' 🚩'
          flags ++
          flagsLeft.innerHTML = bombAmount- flags
          checkForWin()
        } else {
          square.classList.remove('flag')
          square.innerHTML = ''
          flags --
          flagsLeft.innerHTML = bombAmount- flags
        }
      }
    }
 

    //открываем ячейку
    function click(square) {
      let currentId = square.id
      totalClicks++
      if (totalClicks == 1) {
        const resetButton = document.createElement("button")
        resetButton.innerHTML="Начать игру заново"
        resetButton.style.outline="none"
        const header = document.querySelector(".header")
        header.appendChild(resetButton)
        
        resetButton.addEventListener('click', ()=> {
          for (let j = 0; j <squares.length; j++){
            grid.removeChild(squares[j])
            
          }
              createBoard()
            })

      }
      if (isGameOver) return
      if (square.classList.contains('checked') || square.classList.contains('flag')) return
      //вариант 1: в ячейке бомба
      if (square.classList.contains('bomb')) {
        gameOver(square)
        
      } else {
        let total = square.getAttribute('data')
        //вариант 2: бомбы в соседних ячейках. Отображаем их количество
        if (total !=0) {
          square.classList.add('checked')
          if (total == 1) square.classList.add('one')
          if (total == 2) square.classList.add('two')
          if (total == 3) square.classList.add('three')
          if (total == 4) square.classList.add('four')
          square.innerHTML = total
          return
        }
        checkSquare(square, currentId)
      }
      square.classList.add('checked')
    }
  
  
    //проверка соседних ячеек после клика
    function checkSquare(square, currentId) {
      const isLeftEdge = (currentId % width === 0)
      const isRightEdge = (currentId % width === width -1)
  
      setTimeout(() => {
        if (currentId > 0 && !isLeftEdge) {
          const newId = squares[parseInt(currentId) -1].id
          const newSquare = document.getElementById(newId)
          click(newSquare)
        }
        if (currentId > 7 && !isRightEdge) {
          const newId = squares[parseInt(currentId) +1 -width].id
          const newSquare = document.getElementById(newId)
          click(newSquare)
        }
        if (currentId > 8) {
          const newId = squares[parseInt(currentId -width)].id
          const newSquare = document.getElementById(newId)
          click(newSquare)
        }
        if (currentId > 9 && !isLeftEdge) {
          const newId = squares[parseInt(currentId) -1 -width].id
          const newSquare = document.getElementById(newId)
          click(newSquare)
        }
        if (currentId < 62 && !isRightEdge) {
          const newId = squares[parseInt(currentId) +1].id
          const newSquare = document.getElementById(newId)
          click(newSquare)
        }
        if (currentId < 56 && !isLeftEdge) {
          const newId = squares[parseInt(currentId) -1 +width].id
          const newSquare = document.getElementById(newId)
          click(newSquare)
        }
        if (currentId < 54 && !isRightEdge) {
          const newId = squares[parseInt(currentId) +1 +width].id
          const newSquare = document.getElementById(newId)
          click(newSquare)
        }
        if (currentId < 55) {
          const newId = squares[parseInt(currentId) +width].id
          const newSquare = document.getElementById(newId)
          click(newSquare)
        }
      }, 10)
    }
  
    //игра окончена
    function gameOver(square) {
      result.innerHTML = 'Вы проиграли! Игра окончена'
      isGameOver = true
  
      //показать все бомбы
      squares.forEach(square => {
        if (square.classList.contains('bomb')) {
          square.innerHTML = '💣'
          square.classList.remove('bomb')
          square.classList.add('checked')
        }
      })
    }
    //check for win
    function checkForWin() {
     
  
      for (let i = 0; i < squares.length; i++) {
        if (squares[i].classList.contains('flag') && squares[i].classList.contains('bomb')) {
          matches ++
        }
        if (matches === bombAmount) {
          result.innerHTML = 'Победа!'
          isGameOver = true
        }
      }
    }
  })