//TASK 1
//я не поняла, нужно было создавать две кнопки или одну, 
// так что сделала одну, которая меняет название

window.addEventListener("DOMContentLoaded", () => {
    const button = document.querySelector("button");
    let cNum
//событие на первый клик
    button.addEventListener("click", () => {
        if (button.value !=="change"){
            cNum = prompt("Enter your radius")
            button.value="change"
            button.classList.add("pressed")
            button.innerHTML = "Нарисовать"
            button.style.backgroundColor="rgb(255, 94, 0)"
        }
//событие на второй клик
        else{
            const parentDiv = document.createElement("div")
            document.body.append(parentDiv)
            parentDiv.style.width=`${cNum * 10}px`
            parentDiv.style.height=parentDiv.style.width
            parentDiv.classList.add("parentDiv")
            //создание рандомных кружочков
            function random (min, max) {
                return Math.floor(Math.random() * (max - min) ) + min;
            }
                for (let i = 0; i < 100; i++) {
                    const childDiv = document.createElement("div")
                    parentDiv.appendChild(childDiv)
                    childDiv.style.width=`${cNum}px`
                    childDiv.style.height=`${cNum}px`
                    let color1 = random(0, 255);
                    let color2 = random(0, 255);
                    let color3 = random(0, 255);
                    childDiv.style.backgroundColor=`rgb(${color1}, ${color2}, ${color3})`;
                    childDiv.style.borderRadius="50%"
                    childDiv.style.float="left"
                    childDiv.className += "divChild"
                    }
                    //удаление кружков по клику
            const childElem = document.querySelectorAll(".divChild")
            for (let [j] = [0]; [j] < childElem.length; [j++]){
                childElem[j].addEventListener("click", () => {
                parentDiv.removeChild(childElem[j])
            })
        }
    }
    })
})
