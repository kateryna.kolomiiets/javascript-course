/*ЗАДАНИЕ 1. Создайте функцию-конструктор, которая будет создавать объект Human.
Создайте массив объектов и напишите функцию, которая отсортирует их по свойству age

    //создаём функцию-конструктор

const Human = function(name, surname, age) {
    this.name = name;
    this.surname = surname;
    this.age = age;
};
    //создаём с её помощью новые объекты

let human1 = new Human( "Anya", "Ivanova", 26);
let human2 = new Human("Vasya", "Petrov", 24);
let human3 = new Human("Olga", "Sidorova", 85);
let human4 = new Human ("Igor", "Igorev", 58);
let human5 = new Human ("Masha", "Petrenko", 5);

    //Создаём массив из объектов

const humans = [human1, human2, human3, human4, human5];

    //Создаём функцию для сортировки возраста по возрастанию.
    //Если нужно отсортировать по убыванию - меняем условие на a.age < b.age

function sortFromYoungerToOlder(arr) {
    arr.sort((a, b) => a.age > b.age ? 1 : -1);
  }
    //вызываем функцию и выводим отсортированый массив в консоль
sortFromYoungerToOlder(humans);
console.log(humans);
*/





/*ЗАДАНИЕ 2. Создайте функцию-конструктор, которая будет создавать объект Human, 
добавьте на своё усмотрение методы и свойства в этот объект. Подумайте, какие из них стоит
сделать на уровек экземпляра, а какие на уровне функции-конструктора*/
/*
class Human {
    constructor(name,surname,job) {
        //методы и свойства общие для всех объектов
        this.name = name;
        this.surname = surname;
        this.job = job;
        this.sayHello = function(){
            alert(`Hello, ${this.name} ${this.surname}! Long time no see :-)`)
    };
        this.congratulateWithTheNewJob = function(){
            alert(`Congratulations on your new position as ${this.job}! We are happy to see you as a part of our team ;-)`)
    }
};
};
human1 = new Human("Alex", "White","Project Manager");

human2 = new Human("Siri", "Larsen", "Administrator");
//методы и свойства только этого объекта
//создаем массив задач
human2.tasks = ["organizing a meeting", "welcoming a new colleague", "ordering a pizza", "writing a report", "refilling kitchen supplies"];
//создаем функцию, которая выбирает и выводит рандомное задание
const randomElement = human2.tasks[Math.floor(Math.random() * human2.tasks.length)];
human2.yourNextTaskIs = function(){
    alert(`Your next working task will be ${randomElement}. Chop-chop, time is money!`)
}
human3 = new Human("Olav", "Andersen", "Journalist");
human4 = new Human("Maggie", "Smith", "Student");
human5 = new Human("Astrid", "Hansen", "High School Student");
//методы и свойства только этого объекта
//создаём функцию для рандомного выбора оценок из массива
human5.project = "algebra test";
human5.grade = [1, 2, 3, 4, 5];
const randomGrade = human5.grade[Math.floor(Math.random()*human5.grade.length)]
//в зависимости от оценки выведется разный текст сообщения
human5.yourCurrentGradeIs = function(){
    if(randomGrade <= 3) {
    alert(`Dear ${this.name}, your grade for the ${human5.project} is ${randomGrade}. Better luck next time!`)
    }
    else{
        alert(`Dear ${this.name}, your grade for the ${human5.project} is ${randomGrade}. Keep on great job!`)
    };
};

//вызываем разные методы на объектах
human2.sayHello()
human2.congratulateWithTheNewJob();
human2.yourNextTaskIs();

human5.sayHello()
human5.congratulateWithTheNewJob();
human5.yourCurrentGradeIs();
*/